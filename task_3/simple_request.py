import requests
from tqdm import tqdm


def main(url_address):
    for _ in tqdm(range(1000)):
        requests.get(url_address)


if __name__ == "__main__":
    url = input("Enter the URL: ")
    main(url)
