import asyncio
from aiohttp import ClientSession
from tqdm import tqdm


async def fetch(url_address, session):
    async with session.get(url_address) as response:
        return await response.text()


async def run(r, url_address):
    tasks = []
    async with ClientSession() as session:
        for _ in tqdm(range(r)):
            task = asyncio.ensure_future(fetch(url_address, session))
            tasks.append(task)
        await asyncio.gather(*tasks)


def main(url_address):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(1000, url_address))
    loop.close()


if __name__ == "__main__":
    url = input("Введите URL: ")
    main(url)