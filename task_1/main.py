class Solution:
    output = ''
    ANSI_CODE = "\033["
    RED_COLOR = "31m"
    WHITE_COLOR = "37m"
    RED = 'red'
    WHITE = 'white'
    RESET = '\033[0m'

    def __init__(self, red_number, white_number):
        self.red_number = red_number
        self.white_number = white_number

    def colorful_text(self, text, color=WHITE_COLOR, is_bold=True, reset=RESET):
        return f'{self.ANSI_CODE}{1 if is_bold else 0};{color}{text}{reset}'

    def in_a_row(self, number, maximum_color=RED_COLOR, minimum_color=WHITE_COLOR):
        for i in range(number):
            self.output += self.colorful_text('▉', color=maximum_color) + self.colorful_text('▉', color=minimum_color)
        return self.output

    def one_way(self, maximum_number, maximum_color, minimum_number, minimum_color, add_to_end=False):
        self.output += self.colorful_text('▉', color=maximum_color) + self.colorful_text('▉', color=minimum_color)
        maximum_number, minimum_number = maximum_number - 1, minimum_number - 1
        the_rest = maximum_number
        difference = maximum_number - minimum_number
        for _ in range(difference):
            self.output += self.colorful_text('▉▉', color=maximum_color) + \
                           self.colorful_text('▉', color=minimum_color)
        the_rest -= 2 * difference
        self.in_a_row(the_rest, maximum_color, minimum_color)
        if add_to_end:
            self.output += self.colorful_text('▉', color=maximum_color)
        return self.output

    def solution(self, red_number, white_number):
        if white_number < red_number <= white_number * 2:
            add_to_end = False
            if red_number == white_number * 2:
                add_to_end = True
                red_number -= 1
            self.output = self.one_way(maximum_color=self.RED_COLOR,
                                       minimum_color=self.WHITE_COLOR,
                                       maximum_number=red_number,
                                       minimum_number=white_number,
                                       add_to_end=add_to_end)
            return self.output

        elif red_number < white_number <= red_number * 2:
            add_to_end = False
            if red_number * 2 == white_number:
                add_to_end = True
                white_number -= 1
            output = self.one_way(maximum_color=self.WHITE_COLOR,
                                  minimum_color=self.RED_COLOR,
                                  maximum_number=white_number,
                                  minimum_number=red_number,
                                  add_to_end=add_to_end)
            return output
        elif white_number == red_number:
            output = self.in_a_row(red_number)
            return output
        else:
            self.output = "Решений нет"
            return self.output

    def display(self):
        print(self.solution(self.red_number, self.white_number))


if __name__ == "__main__":
    a = Solution(10, 8)
    a.display()
